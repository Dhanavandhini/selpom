package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

//import com.yalla.pages.CreateLeadPage;
import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Edit a lead";
		author = "Dhana";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLead()
		.enterFname()
		.clickFind()
		.clickResultingLead()
		.clickEdit()
		.enterCompanyName()
		.clickSubmit();
		
		
		
		
		
		
	}
	
}






