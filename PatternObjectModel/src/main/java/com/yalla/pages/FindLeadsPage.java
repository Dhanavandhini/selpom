package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{ 
	
	public FindLeadsPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]")  WebElement eleFName;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFind;
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a") WebElement eleResultLead;
	
	public FindLeadsPage enterFname() {
		//WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleFName, "Dhana");  
		return this; 
	}
	
	public FindLeadsPage clickFind() {
		//WebElement eleSubmit = locateElement("name", "submitButton");
          click(eleFind);  
          return this;
         
	}
	
	public ViewLeadPage clickResultingLead() {
		click(eleResultLead);
		return new ViewLeadPage();
		
	}
}







