package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpenTapsCrm extends Annotations{ 
	
	public OpenTapsCrm() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID, using="updateLeadForm_companyName")  WebElement eleUpdateCompany;
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit' and @name='submitButton' and @value='Update']") WebElement eleUpdate;
	
	public OpenTapsCrm enterCompanyName() {
		//WebElement eleUpdateCompany = locateElement("id", "updateLeadForm_companyName");
		clearAndType(eleUpdateCompany, "TL1");  
		return this; 
	}
	
	public ViewLeadPage clickSubmit() {
          click(eleUpdate);  
          return new ViewLeadPage();
	}
	
}







