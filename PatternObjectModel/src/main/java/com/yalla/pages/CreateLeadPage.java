package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName")  WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName")  WebElement enterLastName;
	@FindBy(how=How.NAME, using="submitButton") WebElement eleSubmit;
	
	@And("Enter the CompanyName as (.*)")
	public CreateLeadPage enterCompanyName(String data) {
		//WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyName, data);  
		return this; 
	}
	
	@And("Enter the FirstName as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		//WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, data); 
		return this; 
	}
	
	@And("Enter the LastName as (.*)")
	public CreateLeadPage enterLastName(String data) {
		WebElement enterLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(enterLastName, data); 
		return this; 
	}
	
	@When("Click on Submit button")
	public ViewLeadPage clickSubmit() {
		WebElement eleSubmit = locateElement("name", "submitButton");
          click(eleSubmit);  
          return new ViewLeadPage();
	}
	
	@Then("Verify lead is created sucessfully")
	public void VerifyLeadcreated() {
		System.out.println("Lead created successfully");
	}
	
}







