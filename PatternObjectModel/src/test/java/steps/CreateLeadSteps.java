/*package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	ChromeDriver driver;
	@Given("open the browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
	   driver.manage().window().maximize();
	}

	@Given("load the url")
	public void loadTheUrl() {
	    driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUsername(String username) {
		driver.findElementById("username").sendKeys(username);
	}

	@Given("Enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	    
	}

	@Given("Click on Login button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@Given("Click on CRMSFA")
	public void clickOnCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click on Leads")
	public void clickOnLeads() {
		driver.findElementByLinkText("Leads").click();
	}

	@Given("click on Create Lead")
	public void clickOnCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter the FirstName as (.*)")
	public void enterTheFirstName(String firstname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(firstname);
	}

	@Given("Enter the LastName as (.*)")
	public void enterTheLastName(String lastname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastname);
	}

	@Given("Enter the CompanyName as (.*)")
	public void enterTheCompanyName(String companyname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(companyname);
	}

	@When("Click on Submit button")
	public void clickOnSubmitButton() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify lead is created sucessfully")
	public void verifyLeadIsCreatedSucessfully() {
	    System.out.println("Lead created Successfully");
	}

}
*/