Feature: Login for Leaftap

#Background:
#Create a Lead
#Given open the browser
#And maximize the browser
#And load the url

@smoke @reg
Scenario Outline: Positive create lead flow
And Enter the Username as <username>
And Enter the password as <password>
And Click on Login button
And Click on CRMSFA
And Click on Leads
And Click on Create Lead
And Enter the FirstName as <firstname>
And Enter the LastName as <secondname>
And Enter the CompanyName as <companyname>
When Click on Submit button
Then Verify lead is created sucessfully
Examples:
|username|password|firstname|secondname|companyname|
|DemoSalesManager|crmsfa|Dhana|J|TL|
|DemoCSR|crmsfa|Dhana|J|TestLeaf|